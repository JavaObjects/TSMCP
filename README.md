# 腾讯智能制造云平台

#### 开发环境

1. jdk 1.8

2. mysql 5.0 +

3. 阿里云

4. apache-tomcat-9.0.12

5. win10/9/8/7

#### 开发工具

1. IDEA 2019

2. navicat

4. PowerDesigner 15

5. Axure RP 8

6. Xshell 6

7. Postman

8. RedisDesktopManager

#### 版本控制工具

1. git

#### 使用技术

##### 后端 

1. maven

2. mybatis逆向工程

3. springboot + mybatis

4. spring security权限控制

5. redis

6. [Lambda](https://www.runoob.com/java/java8-lambda-expressions.html)

#### 常见问题

##### 运行时redis错误

：[redis]Connection failure occurred. Restarting subscription task after 5000 ms
  
  1.查看SpringBoot配置文件确认本地redis配置是否正确
  
  2.确保redis正确运行在后台
  
  3.缓存已经超出redis服务所规定的订阅缓存限制值，查看redis.conf配置文件:redis.windows.conf。
    这一句：client-output-buffer-limit pubsub 32mb 8mb 60
    Redis订阅客户端订阅buffer超过32M或持续60秒超过8M,订阅立即被关闭！解决改问题把限制值调大即可解决！
    这里设置为：client-output-buffer-limit pubsub 256mb 64mb 60
  
  4.用 redis-server.exe redis.windows.conf 命令启动redis  指定配文件（如果关掉，会出现问题）


#### Session 共享

一般情况下，一个程序为了保证稳定至少要部署两个，构成集群。那么就牵扯到了 Session 共享的问题，不然用户在 8080 登录成功后，后续访问了 8060 服务器，结果又提示没有登录。

简单实现 Session 共享，采用 Redis 来存储

1.导入redis依赖

2. `application.xml` 中新增配置指定 redis 地址以及 session 的存储方式

3.  为主类添加 `@EnableRedisHttpSession` 注解

4.  完成了基于 Redis 的 Session 共享

#### 退出登录

在WebSecurityConfig 的 configure() 方法中，配置

```java
http.logout();
```

Spring Security 的默认退出配置，Spring Security 在退出时候做了这样几件事：

1. 使当前的 session 失效
2. 清除与当前用户有关的 remember-me 记录
3. 清空当前的 SecurityContext
4. 重定向到登录页

![image text](https://wx4.sinaimg.cn/mw690/bf6adc66ly1g8yh0b8e3zj20fl0gmaaj.jpg)

”/admin“对应权限为admin的用户，“/user”对应用户为user的用户

![image text](https://wx3.sinaimg.cn/mw690/bf6adc66ly1g8yhdlqds1j20a502g3yc.jpg)

也可以在控制层使用注解 @PreAuthorize("hasRole('ROLE_XXX')") 来指定这个请求的权限

`@PreAuthorize` 用于判断用户是否有指定权限，没有就不能访问

修改下要访问的接口，`@PreAuthorize("hasPermission('/admin','r')")`

参数1指明了**访问该接口需要的url**，参数2指明了**访问该接口需要的权限**。  即**访问 url 和权限**。

思路如下：

1. 通过 Authentication 取出登录用户的所有 Role
2. 遍历每一个 Role，获取到每个Role的所有 Permission
3. 遍历每一个 Permission，只要有一个 Permission 的 url 和传入的url相同，且该 Permission 中包含传入的权限，返回 true
4. 如果遍历都结束，还没有找到，返回false



#### 自定义 UserDetailsService类

CustomUserDetailsService类 继承 UserDetailsService